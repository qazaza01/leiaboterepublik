package qazaza.leiabot.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "channel")
public class Channel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Size(max = 100)
    @NotNull
    @Column(name = "id_channel", unique = true, length = 100)
    private String idChannel;

    @Size(max = 100)
    @NotNull
    @Column(name = "name_channel", unique = true, length = 100)
    private String nameChannel;

    @Size(max = 100)
    @NotNull
    @Column(name = "id_guild", unique = true,length = 100)
    private String idGuild;

    @Size(max = 100)
    @Column(name = "div_1", unique = true, length = 100)
    private String div1;

    @Size(max = 100)
    @Column(name = "div_2", unique = true, length = 100)
    private String div2;

    @Size(max = 100)
    @Column(name = "div_3", unique = true, length = 100)
    private String div3;

    @Size(max = 100)
    @Column(name = "div_4", unique = true, length = 100)
    private String div4;

    @Size(max = 100)
    @Column(name = "maveric", unique = true, length = 100)
    private String maveric;

    @Size(max = 100)
    @Column(name = "country", length = 100)
    private String country;

    @Column(name = "notify")
    private boolean notify;

}
