package qazaza.leiabot.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "country_battle_history")
public class CountryBattleHistory {

    @Id
    private Long id;

    @Column(name = "time_of_end")
    private Instant timeOfEnd = Instant.now();

    @Column(name = "is_end")
    private boolean isEnd;

}
