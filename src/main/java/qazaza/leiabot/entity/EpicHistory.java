package qazaza.leiabot.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.Instant;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "epic_history")
public class EpicHistory {

    @Id
    private Long id;

    @Column(name = "time_of_epic")
    private Instant timeOfEpic = Instant.now();

}
