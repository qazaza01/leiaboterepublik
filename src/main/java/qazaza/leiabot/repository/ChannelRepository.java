package qazaza.leiabot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qazaza.leiabot.entity.Channel;

import java.util.List;
import java.util.Optional;

@Repository
public interface ChannelRepository extends JpaRepository<Channel, Long> {

    Optional<Channel> getByIdGuild(String idGuild);

    List<Channel> getAllByNotify(boolean notify);

}