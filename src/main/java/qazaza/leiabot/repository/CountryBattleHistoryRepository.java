package qazaza.leiabot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qazaza.leiabot.entity.CountryBattleHistory;

@Repository
public interface CountryBattleHistoryRepository extends JpaRepository<CountryBattleHistory, Long> {
}