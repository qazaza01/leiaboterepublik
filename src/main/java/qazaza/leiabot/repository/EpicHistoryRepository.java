package qazaza.leiabot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import qazaza.leiabot.entity.Channel;
import qazaza.leiabot.entity.EpicHistory;

import java.util.Optional;

@Repository
public interface EpicHistoryRepository extends JpaRepository<EpicHistory, Long> {

}