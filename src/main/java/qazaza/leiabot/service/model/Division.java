package qazaza.leiabot.service.model;

public enum Division {
    DIV1, DIV2, DIV3, DIV4, MAVERIC;
}
