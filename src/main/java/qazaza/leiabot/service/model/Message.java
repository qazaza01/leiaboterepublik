package qazaza.leiabot.service.model;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    private String guild;
    private String channel;
    private String message;

    @Override
    public String toString() {
        return "MyMessage{" +
                "guild='" + guild + '\'' +
                ", channel='" + channel + '\'' +
                ", message='" + message + '\'' +
                '}';
    }

}
