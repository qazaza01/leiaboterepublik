package qazaza.leiabot.service;

import net.dv8tion.jda.api.entities.Message;
import qazaza.leiabot.entity.Channel;
import qazaza.leiabot.service.model.Division;

public interface ChannelService {

    Channel addChannel(Message message);

    void addDivRole(Message message, Division division);

    void setCountry(Message message, String Country);

    Channel getInfoById(Message message);
}
