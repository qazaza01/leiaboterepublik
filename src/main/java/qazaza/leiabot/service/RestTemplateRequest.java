package qazaza.leiabot.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;
import qazaza.leiabot.util.response.ERepublikBattleResponse;

import java.util.Arrays;

@Service
@Slf4j
public class RestTemplateRequest {

    private final static String BATTLE_API = "https://www.erepublik.com/en/military/campaignsJson/list";

    public ERepublikBattleResponse getEpicBattles() {
        try {
            RestTemplate restTemplate = new RestTemplate();
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
            headers.add("user-agent", "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
            HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);
            ResponseEntity<ERepublikBattleResponse> response = restTemplate.exchange(BATTLE_API, HttpMethod.GET, entity, ERepublikBattleResponse.class);
            return response.getBody();
        } catch (HttpClientErrorException ex) {
            log.error(ex.getMessage());
        }
        return null;
    }
}
