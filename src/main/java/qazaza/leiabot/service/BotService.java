package qazaza.leiabot.service;

import net.dv8tion.jda.api.JDA;

public interface BotService {

    void sendMessage(String guild, String channel, String message);

    JDA getJDA();

}

