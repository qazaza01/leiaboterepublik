package qazaza.leiabot.service.impl;

import net.dv8tion.jda.api.entities.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import qazaza.leiabot.entity.Channel;
import qazaza.leiabot.repository.ChannelRepository;
import qazaza.leiabot.service.ChannelService;
import qazaza.leiabot.service.model.Division;

import java.util.Optional;

@Service
public class ChannelServiceImpl implements ChannelService {

    @Autowired
    private ChannelRepository channelRepository;

    @Override
    public Channel addChannel(Message message) {
        Optional<Channel> byIdGuild = channelRepository.getByIdGuild(message.getGuild().getId());
        Channel channel;
        if (byIdGuild.isPresent()){
            channel = byIdGuild.get();
            channel.setIdChannel(message.getChannel().getId());
            channel.setNameChannel(message.getChannel().getName());
        } else {
            channel = new Channel();
            channel.setNotify(true);
            channel.setIdGuild(message.getGuild().getId());
            channel.setIdChannel(message.getChannel().getId());
            channel.setNameChannel(message.getChannel().getName());
        }
        return channelRepository.save(channel);
    }

    @Override
    public void addDivRole(Message message, Division division) {
        Optional<Channel> byIdGuild = channelRepository.getByIdGuild(message.getGuild().getId());
        if (byIdGuild.isPresent()){
            Channel channel = byIdGuild.get();
            switch (division){
                case DIV1:
                    channel.setDiv1(message.getMentionedRoles().get(0).getId());
                    break;
                case DIV2:
                    channel.setDiv2(message.getMentionedRoles().get(0).getId());
                    break;
                case DIV3:
                    channel.setDiv3(message.getMentionedRoles().get(0).getId());
                    break;
                case DIV4:
                    channel.setDiv4(message.getMentionedRoles().get(0).getId());
                    break;
                case MAVERIC:
                    channel.setMaveric(message.getMentionedRoles().get(0).getId());
                    break;
                default:
            }
            channelRepository.save(channel);
        }

    }

    @Override
    public void setCountry(Message message, String country) {
        Optional<Channel> byIdGuild = channelRepository.getByIdGuild(message.getGuild().getId());
        if (byIdGuild.isPresent()){
            Channel channel = byIdGuild.get();
            channel.setCountry(country);
            channelRepository.save(channel);
        }
    }

    @Override
    public Channel getInfoById(Message message) {
        return channelRepository.getByIdGuild(message.getGuild().getId()).get();
    }
}
