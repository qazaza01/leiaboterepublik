package qazaza.leiabot.service.impl;

import net.dv8tion.jda.api.AccountType;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.OnlineStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import qazaza.leiabot.service.BotService;
import qazaza.leiabot.util.BotListener;

import javax.annotation.PostConstruct;
import javax.security.auth.login.LoginException;

@Service
public class BotServiceImpl implements BotService {

    @Value("${discord.key}")
    private String apiKey;

    @Autowired
    private BotListener botListener;

    private JDA jda;

    @PostConstruct
    public void init() throws LoginException {
        jda = JDABuilder.createDefault(apiKey)
                .addEventListeners(botListener)
                .setStatus(OnlineStatus.DO_NOT_DISTURB)
                .build();
    }

    @Override
    public void sendMessage(String guild, String channel, String message) {
            jda
                    .getGuildById(guild)
                    .getTextChannelById(channel)
                    .sendMessage(message)
                    .queue();
    }

    @Override
    public JDA getJDA() {
        return jda;
    }
}
