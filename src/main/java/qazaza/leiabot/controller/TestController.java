package qazaza.leiabot.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import qazaza.leiabot.service.BotService;
import qazaza.leiabot.service.RestTemplateRequest;
import qazaza.leiabot.service.model.Message;

@RestController("/")
@Slf4j
public class TestController {

    @Autowired
    private BotService bot;

    @Autowired
    private RestTemplateRequest restTemplateRequest;

    @RequestMapping(value = "/testmsg", method = RequestMethod.POST)
    public void msgDiscord(@RequestBody Message message) {
        log.info(message.toString());
        bot.sendMessage(message.getGuild(), message.getChannel(), message.getMessage());
    }
}
