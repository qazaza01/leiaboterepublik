package qazaza.leiabot.util;


import net.dv8tion.jda.api.entities.Message;
import net.dv8tion.jda.api.entities.MessageChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import qazaza.leiabot.entity.Channel;
import qazaza.leiabot.service.ChannelService;
import qazaza.leiabot.service.model.Division;

import static java.util.Objects.nonNull;

@Component
public class BotListener extends ListenerAdapter {

    @Autowired
    private ChannelService channelService;

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {
        if (event.getAuthor().isBot()) return;

        Message message = event.getMessage();
        String content = message.getContentRaw();
        if (content.equals("!ping")) {
            MessageChannel channel = event.getChannel();
            channel.sendMessage(message.getAuthor().getAsMention() + " Pong!").queue();
        } else if (content.contains("!config")) {
            MessageChannel channel = event.getChannel();
            if (content.equals("!config setCurrentChannel")) {
                Channel result = channelService.addChannel(message);
                channel.sendMessage(message.getAuthor().getAsMention() + ",  all fine!\n" +
                        "channelID - " + result.getIdChannel() + "\n" +
                        "channel name - " + result.getNameChannel()).queue();
            } else if (content.contains("roleSet")){
                String[] split = content.split(" ");
                channelService.addDivRole(message, Division.valueOf(split[2]));
            } else if (content.equals("!config getInfo")){
                Channel result = channelService.getInfoById(message);
                channel.sendMessage(getInfo(message, result))
                        .queue();
            } else if (content.contains("setCountry")){
                String[] split = content.split(" ");
                channelService.setCountry(message, split[2]);
            }
        }
    }

    private String getInfo(Message message, Channel channel){
        String result = message.getAuthor().getAsMention() + ",  hi\n" +
                "channelID - " + channel.getIdChannel() + "\n" +
                "channel name - " + channel.getNameChannel() + "\n";
        if (StringUtils.isNotBlank(channel.getCountry())){
            result += "country - " + channel.getCountry() + "\n";
        }
        if (StringUtils.isNotBlank(channel.getDiv1()) && nonNull(message.getGuild().getRoleById(channel.getDiv1()))){
            result += "DIV1 - " + message.getGuild().getRoleById(channel.getDiv1()).getAsMention() + "\n";
        }
        if (StringUtils.isNotBlank(channel.getDiv2()) && nonNull(message.getGuild().getRoleById(channel.getDiv2()))){
            result += "DIV2 - " + message.getGuild().getRoleById(channel.getDiv2()).getAsMention() + "\n";
        }
        if (StringUtils.isNotBlank(channel.getDiv3()) && nonNull(message.getGuild().getRoleById(channel.getDiv3()))){
            result += "DIV3 - " + message.getGuild().getRoleById(channel.getDiv3()).getAsMention() + "\n";
        }
        if (StringUtils.isNotBlank(channel.getDiv4()) && nonNull(message.getGuild().getRoleById(channel.getDiv4()))){
            result += "DIV4 - " + message.getGuild().getRoleById(channel.getDiv4()).getAsMention() + "\n";
        }
        if (StringUtils.isNotBlank(channel.getMaveric()) && nonNull(message.getGuild().getRoleById(channel.getMaveric()))){
            result += "MAVERIC - " + message.getGuild().getRoleById(channel.getMaveric()).getAsMention() + "\n";
        }
        return result;
    }
}
