package qazaza.leiabot.util.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;
import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ERepublikBattleResponse implements Serializable {

    @JsonProperty("time")
    private String time;

    @JsonProperty("last_updated")
    private String lastUpdated;

    @JsonProperty("battles")
    private Map<String, Battle> battles;

    @JsonProperty("countries")
    private Map<String, Country> countries;

}