package qazaza.leiabot.util.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Country {

    @JsonProperty("id")
    private Long id;

    @JsonProperty("name")
    private String name;
}
