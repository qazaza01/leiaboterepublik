package qazaza.leiabot.util.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Map;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Battle {
    @JsonProperty("id")
    private Long id;

    @JsonProperty("div")
    private Map<String, Div> div;

    @JsonProperty("start")
    private String start;

    @JsonProperty("def")
    private Def def;

    @JsonProperty("inv")
    private Inf inv;

}
