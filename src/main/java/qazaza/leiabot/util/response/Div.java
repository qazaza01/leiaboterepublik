package qazaza.leiabot.util.response;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Div {

    @JsonProperty("id")
    private Long id;
    @JsonProperty("div")
    private Long div;
    @JsonProperty("division_end")
    private boolean divisionEnd;
    @JsonProperty("intensity_scale")
    private String intensityScale;
}
