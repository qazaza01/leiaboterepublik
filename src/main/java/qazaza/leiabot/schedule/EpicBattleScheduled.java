package qazaza.leiabot.schedule;

import net.dv8tion.jda.api.JDA;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import qazaza.leiabot.entity.Channel;
import qazaza.leiabot.entity.EpicHistory;
import qazaza.leiabot.repository.ChannelRepository;
import qazaza.leiabot.repository.EpicHistoryRepository;
import qazaza.leiabot.service.BotService;
import qazaza.leiabot.service.RestTemplateRequest;
import qazaza.leiabot.util.response.Battle;
import qazaza.leiabot.util.response.Div;
import qazaza.leiabot.util.response.ERepublikBattleResponse;

import java.time.Instant;
import java.util.List;

import static java.util.Objects.nonNull;

@Component
@EnableAsync
public class EpicBattleScheduled {

    private static final String BATTLE_URL = "https://www.erepublik.com/en/military/battlefield/";

    @Autowired
    private BotService bot;

    @Autowired
    private RestTemplateRequest restTemplateRequest;

    @Autowired
    private EpicHistoryRepository epicHistoryRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Async
    @Scheduled(fixedRate = 10000) // every min
    public void checkOnEpicBattle() {
        ERepublikBattleResponse epicBattles = restTemplateRequest.getEpicBattles();
        for (Battle battle : epicBattles.getBattles().values()) {
            for (Div div : battle.getDiv().values()) {
                if (!div.getIntensityScale().equalsIgnoreCase("cold_war")
                        && !div.getIntensityScale().equalsIgnoreCase("full_scale")
                        && !epicHistoryRepository.existsById(div.getId())) {

                    EpicHistory epicHistory = new EpicHistory(div.getId(), Instant.now());
                    epicHistoryRepository.save(epicHistory);

                    List<Channel> channels = channelRepository.getAllByNotify(true);
                    JDA jda = bot.getJDA();
                    for (Channel channel : channels) {
                        if (StringUtils.isNotBlank(channel.getDiv1()) &&
                                StringUtils.isNotBlank(channel.getDiv2()) &&
                                StringUtils.isNotBlank(channel.getDiv3()) &&
                                StringUtils.isNotBlank(channel.getDiv4())
                        ) {
                            String textNotification = "Hey, ";
                            if (StringUtils.isNotBlank(channel.getDiv1())
                                    && nonNull(jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv1()))
                                    && div.getDiv() == 1) {
                                textNotification += jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv1()).getAsMention();
                            } else if (StringUtils.isNotBlank(channel.getDiv2())
                                    && nonNull(jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv2()))
                                    && div.getDiv() == 2) {
                                textNotification += jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv2()).getAsMention();
                            } else if (StringUtils.isNotBlank(channel.getDiv3())
                                    && nonNull(jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv3()))
                                    && div.getDiv() == 3) {
                                textNotification += jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv3()).getAsMention();
                            } else if (StringUtils.isNotBlank(channel.getDiv4())
                                    && nonNull(jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv4()))
                                    && div.getDiv() == 4) {
                                textNotification += jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getDiv4()).getAsMention();
                            }
                            textNotification += "\nEPIC is here - " + BATTLE_URL + battle.getId();
                            textNotification += "\nTake your guns and let`s go";
                            bot.sendMessage(channel.getIdGuild(), channel.getIdChannel(), textNotification);
                        }
                    }
                }
            }
        }
    }
}
