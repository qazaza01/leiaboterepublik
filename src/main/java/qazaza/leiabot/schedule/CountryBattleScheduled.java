package qazaza.leiabot.schedule;

import net.dv8tion.jda.api.JDA;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import qazaza.leiabot.entity.Channel;
import qazaza.leiabot.entity.CountryBattleHistory;
import qazaza.leiabot.repository.ChannelRepository;
import qazaza.leiabot.repository.CountryBattleHistoryRepository;
import qazaza.leiabot.service.BotService;
import qazaza.leiabot.service.RestTemplateRequest;
import qazaza.leiabot.util.response.Battle;
import qazaza.leiabot.util.response.Country;
import qazaza.leiabot.util.response.Div;
import qazaza.leiabot.util.response.ERepublikBattleResponse;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

import static java.util.Objects.nonNull;

@Component
@EnableAsync
public class CountryBattleScheduled {

    private static final String BATTLE_URL = "https://www.erepublik.com/en/military/battlefield/";

    @Autowired
    private BotService bot;

    @Autowired
    private RestTemplateRequest restTemplateRequest;

    @Autowired
    private CountryBattleHistoryRepository countryBattleHistoryRepository;

    @Autowired
    private ChannelRepository channelRepository;

    @Async
    @Scheduled(fixedRate = 10000) // every min
    public void checkOnEpicBattle() {
        ERepublikBattleResponse battles = restTemplateRequest.getEpicBattles();
        List<Channel> channels = channelRepository.getAllByNotify(true);
        Map<String, Long> countryIds = getCountryId(battles.getCountries(), channels);
        System.out.println(countryIds + "MAPA");
        LocalDateTime timeRequest = new Timestamp(Long.parseLong(battles.getTime())).toLocalDateTime();
        for (Battle battle : battles.getBattles().values()) {
            try {
                if (nonNull(battle)
                        && (countryIds.containsValue(battle.getDef().getId()) || countryIds.containsValue(battle.getInv().getId()))) {
                    LocalDateTime localDateTime = new Timestamp(Long.parseLong(battle.getStart())).toLocalDateTime();
                    boolean checkOnEndBattle = true;
                    for (Div div : battle.getDiv().values()) {
                        checkOnEndBattle = checkOnEndBattle && div.isDivisionEnd();
                    }
                    if (!checkOnEndBattle
                            && timeRequest.isAfter(localDateTime)
                            && (!countryBattleHistoryRepository.existsById(battle.getId())
                            || countryBattleHistoryRepository.findById(battle.getId()).get().isEnd())) {
                        Optional<CountryBattleHistory> byId = countryBattleHistoryRepository.findById(battle.getId());
                        if (byId.isPresent()) {
                            CountryBattleHistory countryBattleHistory = byId.get();
                            countryBattleHistory.setTimeOfEnd(Instant.now());
                            countryBattleHistory.setEnd(false);
                            countryBattleHistoryRepository.save(countryBattleHistory);
                        } else {
                            CountryBattleHistory countryBattleHistory = new CountryBattleHistory(battle.getId(), Instant.now(), false);
                            countryBattleHistoryRepository.save(countryBattleHistory);
                        }
                        JDA jda = bot.getJDA();
                        for (Channel channel : channels) {
                            if ((battle.getDef().getId().equals(countryIds.get(channel.getCountry()))
                                    || battle.getInv().getId().equals(countryIds.get(channel.getCountry())))
                                    && StringUtils.isNotBlank(channel.getMaveric())
                                    && nonNull(jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getMaveric()))) {
                                String textNotification = "Hey!" + jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getMaveric()).getAsMention()
                                        + "\nIn here started next round"
                                        + "\n" + BATTLE_URL + battle.getId();
                                bot.sendMessage(channel.getIdGuild(), channel.getIdChannel(), textNotification);
                            }
                        }
                    } else if (checkOnEndBattle && (!countryBattleHistoryRepository.existsById(battle.getId())
                            || !countryBattleHistoryRepository.findById(battle.getId()).get().isEnd())) {
                        Optional<CountryBattleHistory> byId = countryBattleHistoryRepository.findById(battle.getId());
                        if (byId.isPresent()) {
                            CountryBattleHistory countryBattleHistory = byId.get();
                            countryBattleHistory.setEnd(true);
                            countryBattleHistory.setTimeOfEnd(Instant.now());
                            countryBattleHistoryRepository.save(countryBattleHistory);
                        } else {
                            CountryBattleHistory countryBattleHistory = new CountryBattleHistory(battle.getId(), Instant.now(), true);
                            countryBattleHistoryRepository.save(countryBattleHistory);
                        }
                        JDA jda = bot.getJDA();
                        for (Channel channel : channels) {
                            System.out.println(countryIds.get(channel.getCountry()) + "getCountry");
                            if ((battle.getDef().getId().equals(countryIds.get(channel.getCountry()))
                                    || battle.getInv().getId().equals(countryIds.get(channel.getCountry())))
                                    && StringUtils.isNotBlank(channel.getMaveric())
                                    && nonNull(jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getMaveric()))) {
                                String textNotification = "Hey!" + jda.getGuildById(channel.getIdGuild()).getRoleById(channel.getMaveric()).getAsMention()
                                        + "\nIn here battle is finished"
                                        + "\nBe prepared for next round"
                                        + "\n" + BATTLE_URL + battle.getId();
                                bot.sendMessage(channel.getIdGuild(), channel.getIdChannel(), textNotification);
                            }
                        }
                    }
                }
            } catch (Exception ex){
                System.out.println(ex.getMessage() + "ERROR");
            }
        }
    }

    private Map<String, Long> getCountryId(Map<String, Country> countries, List<Channel> channels) {
        Map<String, Long> result = new HashMap<>();
        List<String> countryList = channels.stream().map(Channel::getCountry).collect(Collectors.toList());
        for (Country country : countries.values()) {
            if (countryList.contains(country.getName())) {
                result.put(country.getName(), country.getId());
            }
        }
        return result;
    }
}
